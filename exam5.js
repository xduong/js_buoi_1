/*
 *
 * input: số có 2 chữ số n
 * vd: n = 69
 *
 *
 *
 *
 *
 * todo:
 * tạo biến nhập số tự nhiên có 2 chữ số
 * tính hàng đơn vị : hangDonvi = n%10
 * tính hàng chục: hangChuc = Math.floor(n/10)%10
 *
 *
 *
 *
 *
 *
 *
 *
 *output: result = hangChuc + hangDonvi
 *
 */
var n, hangDonvi, hangChuc, result;

n = 69;
hangDonvi = n % 10;
hangChuc = Math.floor(n / 10) % 10;
result = hangChuc + hangDonvi;
console.log("result: ", result);
