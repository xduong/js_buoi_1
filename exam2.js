/**
 *
 * input: 5 số nguyên dương 
 * vd: x1 = 1; x2 = 2; x3 = 3; x4 = 4; x5 = 9;
 *
 *
 * todo:
 * tạo biến cho 5 số dương (x1; x2; x3; x4; x5) và biến kết quả trung bình (average)
 * tính trung bình cộng của 5 số nguyên dương bằng tổng 5 số đó chia cho 5
 * average = (x1 + x2 + x3 + x4 + x5) / 5;
 *
 *
 *
 *
 *
 *
 *
 * output: average = (x1 + x2 + x3 + x4 + x5)/5
 *
 *
 */
var x1, x2, x3, x4, x5, average;
x1 = 1;
x2 = 2;
x3 = 3;
x4 = 4;
x5 = 9;
average = (x1 + x2 + x3 + x4 + x5) / 5;
console.log("average: ", average);
